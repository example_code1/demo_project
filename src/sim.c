#include <stdio.h>
#include <inttypes.h>
#include <unistd.h>
#include "shell.h"

#define OPCODEI_MASK 0xFC000000
#define OPCODER_MASK 0x0000003F
#define LOW_5			0x0000001F
#define RS_MASK		0x03E00000
#define RT_MASK		0x001F0000
#define RD_MASK		0x0000F800
#define SA_MASK		0x000007C0
#define IMM_MASK		0x0000FFFF
#define SIGN_EXTN_16	0xFFFF0000
#define SIGN_EXTN_8	0xFFFFFF00
#define SIGN_MASK_16	0x00008000
#define SIGN_MASK_32	0x80000000
#define J_TRGT_MASK	0x03FFFFFF
#define HI_32			0xFFFFFFFF00000000
#define LO_32			0x00000000FFFFFFFF
#define LO_BYTE		0xFF000000
#define LO_MID_BYTE	0x00FF0000
#define HI_MID_BYTE	0x0000FF00
#define HI_BYTE		0x000000FF

// Set to 1 to see what instructions are being called
static const int DEBUG = 1;

void J(uint32_t instruction) {

	uint32_t target = (instruction & J_TRGT_MASK) << 2;
	NEXT_STATE.PC = target;
}

void JAL(uint32_t instruction) {

	uint32_t target = (instruction & J_TRGT_MASK) << 2;
	NEXT_STATE.PC = target;
	NEXT_STATE.REGS[31] = CURRENT_STATE.PC + 4;
}

void BEQ(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t target = (instruction & IMM_MASK);
	
	if (CURRENT_STATE.REGS[rs] == CURRENT_STATE.REGS[rt]) {
		NEXT_STATE.PC = target;
	}
	else {
		NEXT_STATE.PC += 4;
	}
}

void BNE(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t target = (instruction & IMM_MASK);
	
	if (CURRENT_STATE.REGS[rs] != CURRENT_STATE.REGS[rt]) {
		NEXT_STATE.PC = target;
	}
	else {
		NEXT_STATE.PC += 4;
	}
}

void BLEZ(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t target = (instruction & IMM_MASK);
	
	if ((CURRENT_STATE.REGS[rs] & SIGN_MASK_32) || 
		 (CURRENT_STATE.REGS[rs] == 0)) {
		NEXT_STATE.PC = target;
	}
	else {
		NEXT_STATE.PC += 4;
	}
}

void BGTZ(uint32_t instruction) {
	
	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t target = (instruction & IMM_MASK);
	
	if ((CURRENT_STATE.REGS[rs] & SIGN_MASK_32) == 0 && 
		 (CURRENT_STATE.REGS[rs] != 0)) {
		NEXT_STATE.PC = target;
	}
	else {
		NEXT_STATE.PC += 4;
	}
}

void ADDI(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t imm = (instruction & IMM_MASK);
	
	NEXT_STATE.REGS[rt] = imm + CURRENT_STATE.REGS[rs];	

	NEXT_STATE.PC += 4;
}

void ADDIU(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t imm = (instruction & IMM_MASK);
	
	NEXT_STATE.REGS[rt] = imm + CURRENT_STATE.REGS[rs];	

	NEXT_STATE.PC += 4;
}

void SLTI(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t imm = (instruction & IMM_MASK);
	
	if (imm & SIGN_MASK_16) {
		imm &= SIGN_EXTN_16;
	}
	
	if ((int32_t)CURRENT_STATE.REGS[rs] - (int32_t)imm < 0) {
		NEXT_STATE.REGS[rt] = 1;
	}
	else {
		NEXT_STATE.REGS[rt] = 0;
	}

	NEXT_STATE.PC += 4;
}

void SLTIU(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t imm = (instruction & IMM_MASK);
	
	if (imm & SIGN_MASK_16) {
		imm &= SIGN_EXTN_16;
	}
	
	if (CURRENT_STATE.REGS[rs] < imm) {
		NEXT_STATE.REGS[rt] = 1;
	}
	else {
		NEXT_STATE.REGS[rt] = 0;
	}

	NEXT_STATE.PC += 4;

}

void ANDI(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t imm = (instruction & IMM_MASK);
	
	NEXT_STATE.REGS[rt] = (imm & CURRENT_STATE.REGS[rs]);
	
	NEXT_STATE.PC += 4;
}

void ORI(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t imm = (instruction & IMM_MASK);
	
	NEXT_STATE.REGS[rt] = (imm | CURRENT_STATE.REGS[rs]);
	
	NEXT_STATE.PC += 4;
}

void XORI(uint32_t instruction) {
	
	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t imm = (instruction & IMM_MASK);
	
	NEXT_STATE.REGS[rt] = (imm ^ CURRENT_STATE.REGS[rs]);
	
	NEXT_STATE.PC += 4;
}

void LUI(uint32_t instruction) {

	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t imm = (instruction & IMM_MASK);
	
	NEXT_STATE.REGS[rt] = (imm << 16);
	
	NEXT_STATE.PC += 4;
}

void LB(uint32_t instruction) {

	uint32_t base = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint16_t offset = (instruction & IMM_MASK);
	
	uint32_t address = (uint32_t)offset + CURRENT_STATE.REGS[base];
	
	if (mem_read_32(address) & 0x0000080) {
		NEXT_STATE.REGS[rt] = ((mem_read_32(address) & LO_BYTE) >> 24) & SIGN_EXTN_8;
	}
	else {
		NEXT_STATE.REGS[rt] = (mem_read_32(address) & LO_BYTE) >> 24;
	}
	
	NEXT_STATE.PC += 4;
}

void LH(uint32_t instruction) {

	uint32_t base = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint16_t offset = (instruction & IMM_MASK);
	
	uint32_t address = (uint32_t)offset + CURRENT_STATE.REGS[base];
	
	NEXT_STATE.REGS[rt] = (mem_read_32(address) & LO_MID_BYTE) >> 8 & 
								 (mem_read_32(address) & LO_BYTE >> 24);
								 
	if (mem_read_32(address) & 0x00000080) {
		NEXT_STATE.REGS[rt] = (mem_read_32(address) & LO_MID_BYTE) >> 8 & 
								 	 (mem_read_32(address) & LO_BYTE >> 24) &
								 	 SIGN_EXTN_16;	
	}
	else {
		NEXT_STATE.REGS[rt] = (mem_read_32(address) & LO_MID_BYTE) >> 8 & 
								 	 (mem_read_32(address) & LO_BYTE >> 24);
	}
	
	NEXT_STATE.PC += 4;
}

void LW(uint32_t instruction) {
	
	uint32_t base = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint16_t offset = (instruction & IMM_MASK);
	
	uint32_t address = (uint32_t)offset + CURRENT_STATE.REGS[base];
	
	NEXT_STATE.REGS[rt] = ((mem_read_32(address) & LO_MID_BYTE) >> 8) & 
								 ((mem_read_32(address) & LO_BYTE) >> 24) &
								 ((mem_read_32(address) & HI_MID_BYTE) << 8) &
								 ((mem_read_32(address) & HI_BYTE) << 24);
								 
	NEXT_STATE.PC += 4;
}

void LBU(uint32_t instruction) {

	uint32_t base = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint16_t offset = (instruction & IMM_MASK);
	
	uint32_t address = (uint32_t)offset + CURRENT_STATE.REGS[base];
	
	NEXT_STATE.REGS[rt] = mem_read_32(address) & LO_BYTE >> 24;
	
	NEXT_STATE.PC += 4;
}

void LHU(uint32_t instruction) {

	uint32_t base = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint16_t offset = (instruction & IMM_MASK);
	
	uint32_t address = (uint32_t)offset + CURRENT_STATE.REGS[base];
	
	NEXT_STATE.REGS[rt] = (mem_read_32(address) & LO_MID_BYTE) >> 8 & 
								 (mem_read_32(address) & LO_BYTE >> 24);
								 
	NEXT_STATE.PC += 4;
}

void SB(uint32_t instruction) {

	uint32_t base = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint16_t offset = (instruction & IMM_MASK);
	
	uint32_t address = (uint32_t)offset + CURRENT_STATE.REGS[base];
	
	mem_write_32(address, CURRENT_STATE.REGS[rt] & 0xFF);
	
	NEXT_STATE.PC += 4;
}

void SH(uint32_t instruction) {

	uint32_t base = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint16_t offset = (instruction & IMM_MASK);
	
	uint32_t address = (uint32_t)offset + CURRENT_STATE.REGS[base];
	
	mem_write_32(address, CURRENT_STATE.REGS[rt] & 0xFFFF);
	
	NEXT_STATE.PC += 4;
}

void SW(uint32_t instruction) {
	
	uint32_t base = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint16_t offset = (instruction & IMM_MASK);
	
	uint32_t address = (uint32_t)offset + CURRENT_STATE.REGS[base];
	
	mem_write_32(address, CURRENT_STATE.REGS[rt]);
	
	NEXT_STATE.PC += 4;
}

void BLTZ(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t target = (instruction & IMM_MASK);
	
	if (CURRENT_STATE.REGS[rs] & SIGN_MASK_32) {
		NEXT_STATE.PC = target;
	}
	else {
		NEXT_STATE.PC += 4;
	}

}

void BGEZ(uint32_t instruction) {
	
	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t target = (instruction & IMM_MASK);
	
	if ((CURRENT_STATE.REGS[rs] & SIGN_MASK_32) == 0) {
		NEXT_STATE.PC = target;
	}
	else {
		NEXT_STATE.PC += 4;
	}
}

void BLTZAL(uint32_t instruction) {
	
	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t target = (instruction & IMM_MASK);
	
	if (CURRENT_STATE.REGS[rs] & SIGN_MASK_32) {
		NEXT_STATE.PC = target;
	}
	else {
		NEXT_STATE.PC += 4;
	}
	NEXT_STATE.REGS[31] = CURRENT_STATE.PC + 4;
}

void BGEZAL(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t target = (instruction & IMM_MASK);
	
	if ((CURRENT_STATE.REGS[rs] & SIGN_MASK_32) == 0) {
		NEXT_STATE.PC = target;
	}
	else {
		NEXT_STATE.PC += 4;
	}
	NEXT_STATE.REGS[31] = CURRENT_STATE.PC + 4;
}

void SLL(uint32_t instruction) {
	
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	uint32_t sa = (instruction & SA_MASK) >> 6;
	
	NEXT_STATE.REGS[rd] = (CURRENT_STATE.REGS[rt] << CURRENT_STATE.REGS[sa]);

	NEXT_STATE.PC += 4;
}

void SRL(uint32_t instruction) {

	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	uint32_t sa = (instruction & SA_MASK) >> 6;
	
	NEXT_STATE.REGS[rd] = (CURRENT_STATE.REGS[rt] >> CURRENT_STATE.REGS[sa]);

	NEXT_STATE.PC += 4;
}

void SRA(uint32_t instruction) {

	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	uint32_t sa = (instruction & SA_MASK) >> 6;
	
	if (CURRENT_STATE.REGS[rt] & SIGN_MASK_32) {
		NEXT_STATE.REGS[rd] = (CURRENT_STATE.REGS[rt] >> CURRENT_STATE.REGS[sa]) &
									(0xFFFFFFFF << (32 - CURRENT_STATE.REGS[sa]));
	}
	else {
		NEXT_STATE.REGS[rd] = (CURRENT_STATE.REGS[rt] >> CURRENT_STATE.REGS[sa]);
	}

	NEXT_STATE.PC += 4;
}

void SLLV(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	NEXT_STATE.REGS[rd] = (CURRENT_STATE.REGS[rt] << (CURRENT_STATE.REGS[rs] & LOW_5));

	NEXT_STATE.PC += 4;
}

void SRLV(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	NEXT_STATE.REGS[rd] = (CURRENT_STATE.REGS[rt] >> (CURRENT_STATE.REGS[rs] & LOW_5));

	NEXT_STATE.PC += 4;

}

void SRAV(uint32_t instruction) {


	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	if (CURRENT_STATE.REGS[rt] & SIGN_MASK_32) {
		NEXT_STATE.REGS[rd] = (CURRENT_STATE.REGS[rt] >> (CURRENT_STATE.REGS[rs] & LOW_5) &
									(0xFFFFFFFF << (32 - (CURRENT_STATE.REGS[rs] & LOW_5))));
	}
	else {
		NEXT_STATE.REGS[rd] = (CURRENT_STATE.REGS[rt] >> (CURRENT_STATE.REGS[rs] & LOW_5));
	}

	NEXT_STATE.PC += 4;
}

void JR(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	NEXT_STATE.PC = CURRENT_STATE.REGS[rs];
}

void JALR(uint32_t instruction) {
	
	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	if (rd == 0) {
		rd = 31;
	}
	NEXT_STATE.PC = CURRENT_STATE.REGS[rs];
	NEXT_STATE.REGS[rd] = CURRENT_STATE.PC + 4;
}

void ADD(uint32_t instruction) {
	
	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	NEXT_STATE.REGS[rd] = (int32_t)CURRENT_STATE.REGS[rs] + (int32_t)CURRENT_STATE.REGS[rt];	

	NEXT_STATE.PC = CURRENT_STATE.PC + 4;
}

void ADDU(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] + CURRENT_STATE.REGS[rt];	

	NEXT_STATE.PC += 4;
}

void SUB(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	NEXT_STATE.REGS[rd] = (int32_t)CURRENT_STATE.REGS[rs] - (int32_t)CURRENT_STATE.REGS[rt];	

	NEXT_STATE.PC += 4;
}

void SUBU(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] - CURRENT_STATE.REGS[rt];	

	NEXT_STATE.PC += 4;
}

void AND(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] & CURRENT_STATE.REGS[rt];	

	NEXT_STATE.PC += 4;
}

void OR(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] | CURRENT_STATE.REGS[rt];	

	NEXT_STATE.PC += 4;
}

void XOR(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	NEXT_STATE.REGS[rd] = CURRENT_STATE.REGS[rs] ^ CURRENT_STATE.REGS[rt];	

	NEXT_STATE.PC += 4;
}

void NOR(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	NEXT_STATE.REGS[rd] = ~(CURRENT_STATE.REGS[rs] | CURRENT_STATE.REGS[rt]);	

	NEXT_STATE.PC += 4;
}

void SLT(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	if ((int32_t)CURRENT_STATE.REGS[rs] - (int32_t)CURRENT_STATE.REGS[rt] < 0) {
		NEXT_STATE.REGS[rd] = 1;
	}
	else {
		NEXT_STATE.REGS[rd] = 0;
	}

	NEXT_STATE.PC += 4;
}

void SLTU(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	if (CURRENT_STATE.REGS[rs] < CURRENT_STATE.REGS[rt]) {
		NEXT_STATE.REGS[rd] = 1;
	}
	else {
		NEXT_STATE.REGS[rd] = 0;
	}

	NEXT_STATE.PC += 4;
}

void MULT(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	
	int32_t rs_contents = CURRENT_STATE.REGS[rs];
	int32_t rt_contents = CURRENT_STATE.REGS[rt];
	
	int64_t result = rs_contents * rt_contents;
	
	NEXT_STATE.HI = (result & HI_32) >> 32;
	NEXT_STATE.LO = result & LO_32;
	
	NEXT_STATE.PC += 4;
}

void MFHI(uint32_t instruction) {

	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	NEXT_STATE.REGS[rd] = CURRENT_STATE.HI;
	
	NEXT_STATE.PC += 4;
}

void MFLO(uint32_t instruction) {
	
	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	NEXT_STATE.REGS[rd] = CURRENT_STATE.HI;
	
	NEXT_STATE.PC += 4;
}

void MTHI(uint32_t instruction) {

	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	CURRENT_STATE.HI = NEXT_STATE.REGS[rd];
	
	NEXT_STATE.PC += 4;
}

void MTLO(uint32_t instruction) {

	uint32_t rd = (instruction & RD_MASK) >> 11;
	
	CURRENT_STATE.LO = NEXT_STATE.REGS[rd];
	
	NEXT_STATE.PC += 4;
}

void MULTU(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	
	uint32_t rs_contents = CURRENT_STATE.REGS[rs];
	uint32_t rt_contents = CURRENT_STATE.REGS[rt];
	
	int64_t result = rs_contents * rt_contents;
	
	NEXT_STATE.HI = (result & HI_32) >> 32;
	NEXT_STATE.LO = result & LO_32;
	
	NEXT_STATE.PC += 4;
}

void DIV(uint32_t instruction) {
	
	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	
	int32_t rs_contents = CURRENT_STATE.REGS[rs];
	int32_t rt_contents = CURRENT_STATE.REGS[rt];
	
	NEXT_STATE.HI = rs_contents % rt_contents;
	NEXT_STATE.LO = rs_contents / rt_contents;
	
	NEXT_STATE.PC += 4;
}

void DIVU(uint32_t instruction) {

	uint32_t rs = (instruction & RS_MASK) >> 21;
	uint32_t rt = (instruction & RT_MASK) >> 16;
	
	uint32_t rs_contents = CURRENT_STATE.REGS[rs];
	uint32_t rt_contents = CURRENT_STATE.REGS[rt];
	
	NEXT_STATE.HI = rs_contents % rt_contents;
	NEXT_STATE.LO = rs_contents / rt_contents;
	
	NEXT_STATE.PC += 4;
}

void SYSCALL(uint32_t instruction) {

	if (CURRENT_STATE.REGS[2] == 10) RUN_BIT = 0;

	NEXT_STATE.PC += 4;
}

void process_instruction()
{
    /* execute one instruction here. You should use CURRENT_STATE and modify
     * values in NEXT_STATE. You can call mem_read_32() and mem_write_32() to
     * access memory. */
   
	// IF
	uint32_t instruction = mem_read_32(CURRENT_STATE.PC);
	uint32_t opcode_i = (instruction & OPCODEI_MASK) >> 26;
	uint32_t opcode_r = instruction & OPCODER_MASK;
	uint32_t regimm = (instruction & RT_MASK) >> 16;
	
	// Register operations
	if (opcode_i == 0) {
		switch(opcode_r) {
			case 0x0000000C:
				if (DEBUG) printf("called syscall...\n");
				SYSCALL(instruction);
				break;
			case 0x00000000:
				if (DEBUG) printf("called sll...\n");
				SLL(instruction);
				break;
			case 0x00000002:
				if (DEBUG) printf("called srl...\n");
				SRL(instruction);
				break;
			case 0x00000003:
				if (DEBUG) printf("called sra...\n");
				SRA(instruction);
				break;
			case 0x00000004:
				if (DEBUG) printf("called sllv...\n");
				SLLV(instruction);
				break;
			case 0x00000006:
				if (DEBUG) printf("called SRLV...\n");
				SRLV(instruction);
				break;
			case 0x00000007:
				if (DEBUG) printf("called SRAV...\n");
				SRAV(instruction);
				break;
			case 0x00000008:
				if (DEBUG) printf("called jr...\n");
				JR(instruction);
				break;
			case 0x00000009:
				if (DEBUG) printf("called jalr...\n");
				JALR(instruction);
				break;
			case 0x00000020:
				if (DEBUG) printf("called add...\n");
				ADD(instruction);
				break;
			case 0x00000021:
				if (DEBUG) printf("called addu...\n");
				ADDU(instruction);
				break;
			case 0x00000022:
				if (DEBUG) printf("called sub...\n");
				SUB(instruction);
				break;
			case 0x00000023:
				if (DEBUG) printf("called subu...\n");
				SUBU(instruction);
				break;
			case 0x00000024:
				if (DEBUG) printf("called and...\n");
				AND(instruction);
				break;
			case 0x00000025:
				if (DEBUG) printf("called or...\n");
				OR(instruction);
				break;
			case 0x00000026:
				if (DEBUG) printf("called xor...\n");
				XOR(instruction);
				break;
			case 0x00000027:
				if (DEBUG) printf("called nor...\n");
				NOR(instruction);
				break;
			case 0x0000002A:
				if (DEBUG) printf("called slt...\n");
				SLT(instruction);
				break;
			case 0x0000002B:
				if (DEBUG) printf("called sltu...\n");
				SLTU(instruction);
				break;
			case 0x00000018:
				if (DEBUG) printf("called mult...\n");
				MULT(instruction);
				break;
			case 0x00000010:
				if (DEBUG) printf("called mfhi...\n");
				MFHI(instruction);
				break;
			case 0x00000012:
				if (DEBUG) printf("called mflo...\n");
				MFLO(instruction);
				break;
			case 0x00000011:
				if (DEBUG) printf("called mthi...\n");
				MTHI(instruction);
				break;
			case 0x00000013:
				if (DEBUG) printf("called mtlo...\n");
				MTLO(instruction);
				break;
			case 0x00000019:
				if (DEBUG) printf("called multu...\n");
				MULTU(instruction);
				break;
			case 0x0000001A:
				if (DEBUG) printf("called div...\n");
				DIV(instruction);
				break;
			case 0x0000001B:
				if (DEBUG) printf("called divu...\n");
				DIVU(instruction);
				break;
		}
	}
	// Branch instructions
	else if (opcode_i == 1) {
		switch(regimm) {
			case 0x00000001:
				if (DEBUG) printf("called bgez...\n");
				BGEZ(instruction);
				break;
			case 0x00000010:
				if (DEBUG) printf("called bltzal...\n");
				BLTZAL(instruction);
				break;
			case 0x00000011:
				if (DEBUG) printf("called BGEZAL...\n");
				BGEZAL(instruction);
				break;
		}
	}
	// Immediate instructions
	else {
		switch(opcode_i) {
			case 0x00000002:
				if (DEBUG) printf("called j...\n");
				J(instruction);
				break;
			case 0x00000003:
				if (DEBUG) printf("called jal...\n");
				JAL(instruction);
				break;
			case 0x00000004:
				if (DEBUG) printf("called beq...\n");
				BEQ(instruction);
				break;
			case 0x00000005:
				if (DEBUG) printf("called bne...\n");
				BNE(instruction);
				break;
			case 0x00000006:
				if (DEBUG) printf("called blez...\n");
				BLEZ(instruction);
				break;
			case 0x00000007:
				if (DEBUG) printf("called bgtz...\n");
				BGTZ(instruction);
				break;
			case 0x00000008:
				if (DEBUG) printf("called addi...\n");
				ADDI(instruction);
				break;
			case 0x00000009:
				if (DEBUG) printf("called addiu...\n");
				ADDIU(instruction);
				break;
			case 0x0000000A:
				if (DEBUG) printf("called slti...\n");
				SLTI(instruction);
				break;
			case 0x0000000B:
				if (DEBUG) printf("called sltiu...\n");
				SLTIU(instruction);
				break;
			case 0x0000000C:
				if (DEBUG) printf("called ANDI...\n");
				ANDI(instruction);
				break;
			case 0x0000000D:
				if (DEBUG) printf("called ori...\n");
				ORI(instruction);
				break;
			case 0x0000000E:
				if (DEBUG) printf("called xori...\n");
				XORI(instruction);
				break;
			case 0x0000000F:
				if (DEBUG) printf("called lui...\n");
				LUI(instruction);
				break;
			case 0x00000020:
				if (DEBUG) printf("called lb...\n");
				LB(instruction);
				break;
			case 0x00000021:
				if (DEBUG) printf("called lh...\n");
				LH(instruction);
				break;
			case 0x00000023:
				if (DEBUG) printf("called lw...\n");
				LW(instruction);
				break;
			case 0x00000024:
				if (DEBUG) printf("called lbu...\n");
				LBU(instruction);
				break;
			case 0x00000025:
				if (DEBUG) printf("called lhu...\n");
				LHU(instruction);
				break;
			case 0x00000028:
				if (DEBUG) printf("called sb...\n");
				SB(instruction);
				break;
			case 0x00000029:
				if (DEBUG) printf("called sh...\n");
				SH(instruction);
				break;
			case 0x0000002B:
				if (DEBUG) printf("called sw...\n");
				SW(instruction);
				break;
		}
	}  
}
